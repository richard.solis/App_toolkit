import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SessionService } from '../services/session.service';
import { Services } from '../services/services.service';

import { SocialSharing } from '@ionic-native/social-sharing';
import { Camera } from '@ionic-native/camera';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { HiringPage } from '../pages/hiring/hiring';
import { OnboardingPage } from '../pages/onboarding/onboarding';
import { LeadershipPage } from '../pages/leadership/leadership';
import { TeamsPage } from '../pages/teams/teams';
import { AllsPage } from '../pages/alls/alls';
import { FavoritesPage } from '../pages/favorites/favorites';
import { ToolPage } from '../pages/tool/tool';
import { SearchingPage } from '../pages/searching/searching';
import { TagPage } from '../pages/tag/tag';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { CuriosidadPage } from '../pages/curiosidad/curiosidad';
import { ResultadosPage } from '../pages/resultados/resultados';

//import { IonicImageViewerModule } from 'ionic-img-viewer';

import { ResourcePage } from '../pages/resource/resource';
import { DownloadablePage } from '../pages/downloadable/downloadable';


import { NavBarPage } from '../pages/nav-bar/nav-bar';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    HiringPage,
    OnboardingPage,
    LeadershipPage,
    TeamsPage,
    NavBarPage,
    AllsPage,
    FavoritesPage,
    ToolPage,
    SearchingPage,
    TagPage,
    ProfilePage,
    ResourcePage,
    DownloadablePage,
    RegisterPage,
    CuriosidadPage,
    ResultadosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    HiringPage,
    OnboardingPage,
    LeadershipPage,
    TeamsPage,
    NavBarPage,
    AllsPage,
    FavoritesPage,
    ToolPage,
    SearchingPage,
    TagPage,
    ProfilePage,
    ResourcePage,
    DownloadablePage,
    RegisterPage,
    CuriosidadPage,
    ResultadosPage
  ],
  providers: [
    StatusBar,
    SessionService,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Services,
    SocialSharing,
    Camera,
    GoogleAnalytics,
    PhotoViewer,
    InAppBrowser
  ]
})
export class AppModule {}
