import { Component, ViewChild } from '@angular/core';
import { Nav, MenuController, Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { FavoritesPage } from '../pages/favorites/favorites';
import { Services } from '../services/services.service';
import { SessionService } from '../services/session.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  @ViewChild(Nav) nav: Nav;
  public info;
  public pages: Array<any> = [];
  public menu_items;
  public user = {
    uname: '',
    id: ''
  };

  constructor(
      public platform: Platform, 
      statusBar: StatusBar, 
      splashScreen: SplashScreen, 
      public menuCtrl: MenuController,
      public app: App,
      public Serv: Services,
      public sessionS: SessionService,
      private ga: GoogleAnalytics
    ) {

    this.user = this.sessionS.getObject('toolkit.member');

    platform.ready().then(() => {

      this.ga.startTrackerWithId('UA-68891979-6')
       .then(() => {
         console.log('Google analytics is ready now');
            this.ga.trackView('test');
         // Tracker is ready
         // You can now track pages or set additional information such as AppVersion or UserId
       })
       .catch(e => console.log('Error starting GoogleAnalytics', e));
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    }); 

    // this.Serv.getMenu({
    //       action: 'main_menu',
    //       uid: this.user.id

    //     }, this.sessionS.getItem('token'))
    //     .subscribe(response =>{
    //       // loader.dismiss();
    //       this.menu_items = response;
    //       console.log(this.menu_items);
    //     })

    this.pages = [
      { title: 'Inicio', component: HomePage, icon: 'home'},
      { title: 'Mi Perfil', component: ProfilePage, icon: 'person'},
      { title: 'Cerrar sesión', component: HomePage, icon: 'md-exit'}

    ];

  }

  goHome(page){

    console.log(page.title);

    if(page.title == "Cerrar sesión"){
       this.app.getRootNav().setRoot( LoginPage );
       localStorage.clear();

       // console.log('logout');

    }else if(page.title == "Inicio"){
      this.nav.push(page.component);
      this.menuCtrl.swipeEnable(false);

       // console.log('Inicio');

    }else if(page.title == "Mi Perfil"){

      this.nav.push(page.component);
      this.menuCtrl.swipeEnable(false);

    }

  }

}

