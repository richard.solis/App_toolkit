export class AppSettings {
  public static SERVER = 'https://toolkit.universidadscotiabank.pe/';
  // public static SERVER = 'http://leaderstoolkit.atypax.com/';
  public static SERVER_TOKEN = 'session/token';
  public static HIRINGS = 'hiring.json';
  public static ONBOARDING = 'onboarding.json';
  public static LEADERSHIP = 'autoliderazgo.json';
  public static TEAMS = 'teams.json';
  public static ALLS = 'todos.json';
  public static FAVORITES = 'favorites.json';
  public static TOOL = 'tool.json';
  public static LOGIN = 'scotia/user?_format=json';
  public static CATEGORY = 'app/nodo?_format=json';
  
  public static ID_HOME = '18';
  public static ID_HIRING = '17';
  public static ID_ONBOARDING = '19';
  public static ID_LEADERSHIP = '20';
  public static ID_TEAMS = '21';
  public static ID_CURIOSIDAD = '23';
  public static ID_RESULTADO = '22';

  // public static ID_HOME = '19';
  // public static ID_HIRING = '15';
  // public static ID_ONBOARDING = '16';
  // public static ID_LEADERSHIP = '17';
  // public static ID_TEAMS = '18';

}
