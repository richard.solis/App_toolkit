import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, MenuController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Services } from '../../services/services.service';
import { SessionService } from '../../services/session.service';
import { Http } from '@angular/http';
import { HiringPage } from '../hiring/hiring';
import { OnboardingPage } from '../onboarding/onboarding';
import { LeadershipPage } from '../leadership/leadership';
import { TeamsPage } from '../teams/teams';
import { AllsPage } from '../alls/alls';
import { ToolPage } from '../tool/tool';
import { SearchingPage } from '../searching/searching';
import { CuriosidadPage } from '../curiosidad/curiosidad';
import { ResultadosPage } from '../resultados/resultados';

/**
 * Generated class for the FavoritesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
  providers: [
    Services,
    FormBuilder
  ]
})
export class FavoritesPage {

  public favorites = [];
  public loader = this.loadingCtrl.create({});
  public menu_items = [];
  public user = {
    id: ''
  };

  public userForm:FormGroup;


  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams, 
    public Serv: Services, 
    public http: Http,
    public loadingCtrl: LoadingController,
    public sessionS: SessionService,
    public menu: MenuController,
    public SS: SessionService,
    public _fb: FormBuilder,
    public alertCtrl: AlertController) {
  }

  ngOnInit(){

    this.userForm = this._fb.group({
      text: ['', Validators.required],
      action: 'search'
    });

  }

  ionViewWillEnter() {

    let loader = this.loadingCtrl.create({});
    let category = this.navParams.get('category');
    this.user = this.sessionS.getObject('toolkit.member');

    loader.present();

    this.Serv.getDetailCategory({
          action: 'by_categoria',
          categoryId: category.id,
          uid: this.user.id
        }, this.SS.getItem('token'))
        .subscribe(response =>{

          loader.dismiss();
          this.favorites = response;

          if(this.favorites.length > 0){
            this.favorites.sort((a, b) => {
              if (a.title < b.title) return -1;
              else if (a.title > b.title) return 1;
              else return 0;
            });
          }
          // console.log(this.favorites);
        })

    this.Serv.getMenu({
        action: 'main_menu',
        uid: this.user.id
      }, this.sessionS.getItem('token'))
      .subscribe(response =>{
        // loader.dismiss();
        this.menu_items = response;

        if(this.menu_items.length > 0){
          this.menu_items.sort((a, b) => {
            if (a.id < b.id) return -1;
            else if (a.id > b.id) return 1;
            else return 0;
          });
        }
        // console.log(this.menu_items);
      })
    
  }

  searching(model:any, isValid:Boolean){

    if (isValid) {
      this.navCtrl.push(SearchingPage, {
        text: model
      }) 
    }else{
      let alert = this.alertCtrl.create({
        title: 'Por favor complete el campo de busqueda',
        buttons: ['OK']
      });
      alert.present();
    }
    
  }

  goTool(id){
    console.log(id);

    this.navCtrl.push(ToolPage, {
      id_tool: id
    });
  }

  goCategorieDetail(item) {

    if(item.id == "1"){
      this.navCtrl.push(HiringPage, {
        category: item
      });
    }else if(item.id == "2"){
      this.navCtrl.push(OnboardingPage, {
        category: item
      });
    }else if(item.id == "3"){
      this.navCtrl.push(LeadershipPage, {
        category: item
      });
    }else if(item.id == "4"){
      this.navCtrl.push(TeamsPage, {
        category: item
      });
    }else if(item.id == "22"){
      this.navCtrl.push(ResultadosPage, {
        category: item
      });
    }else if(item.id == "23"){
      this.navCtrl.push(CuriosidadPage, {
        category: item
      });
    }else if(item.id == "all"){
      this.navCtrl.push(AllsPage, {
        category: item
      });
    }else if(item.id == "favorite"){
      this.navCtrl.push(FavoritesPage, {
        category: item
      });
    }
  }

}
