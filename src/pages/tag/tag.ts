import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Slides, AlertController } from 'ionic-angular';
import { Services } from '../../services/services.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Http } from '@angular/http';
import { SessionService } from '../../services/session.service';
import { ToolPage } from '../tool/tool';

/**
 * Generated class for the TagPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tag',
  templateUrl: 'tag.html',
})
export class TagPage {

  public results = [];

  public tag = {};

  public tagName = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public Services: Services, 
    public _fb: FormBuilder,
    public http: Http,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public SS: SessionService
   ) {
  	// this.results = [
   //    {
   //      id: '1',
   //      title: '70:20:10',
   //      body: 'Modelo que te permite definir estrategias de aprendizaje efectivo',
   //      image: 'http://leaderstoolkit.atypax.com/sites/default/files/2017-09/b1_11.jpg'
   //    },
   //    {
   //      id: '2',
   //      title: 'Prejuicios Inconscientes',
   //      body: 'Identifica qué prejuicios inconscientes pueden interferir durante el proceso de selección',
   //      image: 'http://leaderstoolkit.atypax.com/sites/default/files/2017-09/b1_0.jpg'
   //    }
   //  ] 
  }

  ionViewDidLoad() {

    this.tag = this.navParams.get('tag_id');
    this.tagName = this.navParams.get('tag');

    let loader = this.loadingCtrl.create({});
    loader.present();

    // GET DATA TOOL

    this.Services.filterTag({
      action: 'by_tag',
      tagId: this.tag
    }, this.SS.getItem('token'))
      .subscribe(
        response => {
           loader.dismiss();

          console.log(response);

          this.results = response;
          // this.tag = response.tag;
        }
      )

    // console.log(this.tag);

  }

  goTool(id){
    console.log(id);

    this.navCtrl.push(ToolPage, {
      id_tool: id
    });
  }

}
