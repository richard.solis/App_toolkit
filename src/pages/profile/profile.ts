import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SessionService } from '../../services/session.service';
import { Services } from '../../services/services.service';
import * as $ from 'jquery';
import { Content } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [
    Services,
    FormBuilder
  ]
})
export class ProfilePage {

  @ViewChild(Content) content: Content;
  public userForm:FormGroup;
  public contextInputPass; 
  public valueAttr = 'password';

  public iconShowPass = true;
  public iconShowPassNew = true;
  public iconShowPassConfirm = true;

  public iconHiddenPass = false;
  public iconHiddenPassNew = false;
  public iconHiddenPassConfirm = false;

  public showError = false;
  public user_data = {
    uname: '',
    picture: ''
  };

  public image;
  public politica;

  constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public _fb: FormBuilder, 
        public elRef: ElementRef,
        public menu: MenuController,
        public SS: SessionService,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public Serv: Services,
        public app: App,
        private camera: Camera
      ) {
  }

  ionViewDidLoad() {

    let loader = this.loadingCtrl.create({});
    loader.present();


    // console.log('ionViewDidLoad ProfilePage');

      this.Serv.login({
        name: this.user_data.uname,
        pass: atob(this.SS.getItem('token_')),
        action: 'login'
      }, this.SS.getItem('token'))
      .subscribe(response =>{
        // console.log("logueado");
        loader.dismiss();

        this.SS.setObject('toolkit.member', response.user);
        this.image = response.user.picture;

        // if(response.user.politica == '0'){
        //   this.politica = false;
        // }else{
        //   this.politica = true;
        // }

        // console.log('politica',this.politica);

      })
  }

  ngOnInit() {

    this.user_data = this.SS.getObject('toolkit.member');
    let user = this.SS.getObject('toolkit.member');
    // user.politica = this.politica;

    this.menu.close(); 

    
    console.log('politica', user.politica);
    
    this.userForm = this._fb.group({
      // image: user.picture,
      nickname: user.nickname,
      nombre_completo: user.nombre_completo,
      uname: user.uname,
      correo_corp: user.correo_corp,
      correo_electronico_persona: user.correo_electronico_persona,
      compania: user.compania?user.compania : '',
      celular: user.celular,
      fecha_nacimiento: user.fecha_nacimiento,
      area: user.area,
      cargo_puesto: user.cargo_puesto,
      password: '',
      password_new: '',
      password_new_repeat: '',
      politica: (user.politica == "1") ? user.politica = true : user.politica = false,
      action: 'update'

    });

  }

  getPicture(){
    let options: CameraOptions = {
      // sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  getPictureGallery(){
    let options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    })
    .catch(error =>{
      console.error( error );
    });
  }

  showPassToday(){

    let type = $('.pass-input.todayPass input').attr('type');

      if (type == "password") {
        $('.pass-input.todayPass input').attr('type', 'text');
          this.iconShowPass = false;
          this.iconHiddenPass = true;

      } else {
        $('.pass-input.todayPass input').attr('type', 'password');
          this.iconShowPass = true;
          this.iconHiddenPass = false;
      }

  }

  showPassNew(){

    let type = $('.pass-input.newPass input').attr('type');

      if (type == "password") {
        $('.pass-input.newPass input').attr('type', 'text');
          this.iconShowPassNew = false;
          this.iconHiddenPassNew = true;

      } else {
        $('.pass-input.newPass input').attr('type', 'password');
          this.iconShowPassNew = true;
          this.iconHiddenPassNew = false;
      }

  }

  showPassConfirm(){

    let type = $('.pass-input.confirmPass input').attr('type');

      if (type == "password") {
        $('.pass-input.confirmPass input').attr('type', 'text');
          this.iconShowPassConfirm = false;
          this.iconHiddenPassConfirm = true;

      } else {
        $('.pass-input.confirmPass input').attr('type', 'password');
          this.iconShowPassConfirm = true;
          this.iconHiddenPassConfirm = false;
      }

  }

  goBack(){

    this.navCtrl.pop(); 
  
  }

  onSubmit(model:any){

    // console.log(model);
    let loader = this.loadingCtrl.create({});
    loader.present();


    if (model.password_new != '' || model.password_new_repeat != '' || model.password != '') {
       if (model.password_new != model.password_new_repeat || model.password == model.password_new) {

         let alert =  this.alertCtrl.create({
            title: 'Por favor verifique su contraseña',
            buttons: ['OK']
          });

          loader.dismiss();
          alert.present();

       }else{
         if (model.password_new.length < 8 && model.password_new_repeat.length < 8){

           let alert =  this.alertCtrl.create({
            title: 'La contraseña debe de tener más de 8 caracteres e incluír cualquier número.',
            buttons: ['OK']
          });

          loader.dismiss();
          alert.present();

         }else{
            if (model.password_new.search(/[0-9]/) < 0) {

              let alert =  this.alertCtrl.create({
                title: 'La contraseña debe de tener más de 8 caracteres e incluír cualquier número.',
                buttons: ['OK']
              });

              loader.dismiss();
              alert.present();

            }else{

              let params = {
                fields: {
                  image: this.image,
                  uname: model.uname,
                  nickname: model.nickname,
                  correo_electronico_persona: model.correo_electronico_persona,
                  celular: model.celular,
                  fecha_nacimiento: model.fecha_nacimiento,
                  password: model.password,
                  newPassword: model.password_new,
                  politica: model.politica ? model.politica = 1 : model.politica = 0
                },
                action: "update"

              }

              if((this.image).indexOf('base64') == -1){
                delete params.fields.image;
              }

              this.Serv.updateUser(params, this.SS.getItem('token'))
                .subscribe(response => {
                  // console.log(response.message);
                  if(response.message == "Password error"){

                    let alert =  this.alertCtrl.create({
                      title: 'Contraseña actual es incorrecta',
                      buttons: ['OK']
                    });

                    this.content.scrollToTop();

                    loader.dismiss();
                    alert.present();
                    this.ionViewDidLoad();

                  }else{

                    this.content.scrollToTop();

                    let alert =  this.alertCtrl.create({
                      title: 'Edición satisfactoria!',
                      buttons: ['OK']
                    });

                    loader.dismiss();
                    alert.present();
                    this.ionViewDidLoad();

                    this.Serv.login({
                      name: this.user_data.uname,
                      pass: params.fields.newPassword,
                      action: 'login'
                    }, response['_body'])
                    .subscribe(response =>{
                      console.log("logueado");
                      this.SS.setObject('toolkit.member', response.user);
                      this.SS.setItem('token_', btoa(params.fields.newPassword));
                    })
                  }

                })
            }
         }
       }

    } else{
      // console.log('no hay contraseña');

      let params = {
        fields: {
            image: this.image,
            uname: model.uname,
            nickname: model.nickname,
            correo_electronico_persona: model.correo_electronico_persona,
            celular: model.celular,
            fecha_nacimiento: model.fecha_nacimiento,
            politica: model.politica ? model.politica = 1 : model.politica = 0


          },
          action: "update"
      }

      if((this.image).indexOf('base64') == -1){
        delete params.fields.image;
      }

      // console.log(params);

      this.Serv.updateUser(params, this.SS.getItem('token'))
        .subscribe(

          response => {
            console.log(response);


            this.content.scrollToTop();

            let alert =  this.alertCtrl.create({
              title: 'Edición satisfactoria!',
              buttons: ['OK']
            });

            loader.dismiss();
            alert.present();

            this.ionViewDidLoad();

            // this.Serv.login({
            //   name: this.user_data.uname,
            //   pass: params.fields.newPassword,
            //   action: 'login'
            // }, response['_body'])
            // .subscribe(response =>{
            //   console.log("logueado");
            //   this.SS.setObject('toolkit.member', response.user);

            // })
            // this.ngOnInit();

        }, err => {
            this.content.scrollToTop();

            let alert =  this.alertCtrl.create({
              title: 'Edición satisfactoria!',
              buttons: ['OK']
            });

            loader.dismiss();
            alert.present();

            this.ionViewDidLoad();
            
        })
    }

  }



}