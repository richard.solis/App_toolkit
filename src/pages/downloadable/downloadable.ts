import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Slides, AlertController } from 'ionic-angular';
import { Services } from '../../services/services.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Http } from '@angular/http';
import { TagPage } from '../tag/tag';
import { SessionService } from '../../services/session.service';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the DownloadablePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-downloadable',
  templateUrl: 'downloadable.html',
})
export class DownloadablePage {

  public downloadable = {};
  public nid;
  
  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      public Services: Services, 
      public _fb: FormBuilder,
      public http: Http,
      public loadingCtrl: LoadingController,
      public alertCtrl: AlertController,
      public SS: SessionService,
      private socialSharing: SocialSharing

    ) {

  	// this.downloadable = {
  	// 	id: 1,
  	// 	tool: 'Prejuicios inconscientes',
  	// 	category: 'Hiring',
  	// 	title: 'Guía Prejuicios Inconscientes',
  	// 	en_red: 'http://sharepoint/sites/RRHH/Aprendizaje-Comunicaciones/USBP/HiringPrejuiciosInconscientes/Forms/AllItems.aspx',
  	// 	fuera_red: 'http://leaderstoolkit.atypax.com/sites/default/files/2017-09/1.5_Gui%CC%81a%20Prejuicios%20Inconscientes_0.pdf',
  	// }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad DownloadablePage');

    this.nid = this.navParams.get('id');
    
    let loader = this.loadingCtrl.create({});

    // console.log(this.id_tool);
    loader.present();

    this.Services.getDataTool({
      action: 'descargable',
      nodeId: this.nid

    }, this.SS.getItem('token'))
      .subscribe(
        response => {

          loader.dismiss();
          console.log(response);
          this.downloadable = response;
        }
      )
  }

}
