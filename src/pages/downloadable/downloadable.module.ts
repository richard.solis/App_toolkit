import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DownloadablePage } from './downloadable';

@NgModule({
  declarations: [
    DownloadablePage,
  ],
  imports: [
    IonicPageModule.forChild(DownloadablePage),
  ],
})
export class DownloadablePageModule {}
