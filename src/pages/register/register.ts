import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SessionService } from '../../services/session.service';
import { Services } from '../../services/services.service';
import * as $ from 'jquery';
import { Content } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LoginPage} from "../login/login";




/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers: [
    Services,
    FormBuilder
  ]
})
export class RegisterPage {

  @ViewChild(Content) content: Content;
  public userForm:FormGroup;
  public contextInputPass; 
  public valueAttr = 'password';

  public iconShowPass = true;
  public iconHiddenPass = false;

  public showError = false;


  public image;

  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams,
    public _fb: FormBuilder, 
    public elRef: ElementRef,
    public menu: MenuController,
    public SS: SessionService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public Serv: Services,
    public app: App,
    private camera: Camera
  	) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  ngOnInit() {
    // console.log(this.user_data.uname);

    this.menu.close(); 
    
    this.userForm = this._fb.group({
 		username: ['', Validators.required],
 		nombre_completo: ['', Validators.required],
		correo_electronico_personal:  ['', Validators.required],
		password: ['', Validators.required],
      	action: 'sign_up'
    });

  }

  showPassToday(){

    let type = $('.pass-input.todayPass input').attr('type');

      if (type == "password") {
        $('.pass-input.todayPass input').attr('type', 'text');
          this.iconShowPass = false;
          this.iconHiddenPass = true;

      } else {
        $('.pass-input.todayPass input').attr('type', 'password');
          this.iconShowPass = true;
          this.iconHiddenPass = false;
      }
  }

  isEmail(email) {
  	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  	return regex.test(email);
  }
  
  goBack(){
    this.app.getRootNav().setRoot(LoginPage);

  }

  onSubmit(model:any, isValid:Boolean){

  	var regex = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
  	// console.log(model);
  	if (isValid) {

      let loader = this.loadingCtrl.create({});
      
      loader.present();

      if(!this.isEmail(model.correo_electronico_personal)){
      	  
      	  loader.dismiss();

          let alert =  this.alertCtrl.create({
              title: 'Correo electrónico no válido',
              buttons: ['OK']
            });

           alert.present();

      }else{
      	 if (model.password.length < 8){

           let alert =  this.alertCtrl.create({
            title: 'La contraseña debe de tener más de 8 caracteres',
            buttons: ['OK']
          });

          loader.dismiss();
          alert.present();

         }else{

         	if(!regex.test(model.password)){
     		 	let alert =  this.alertCtrl.create({
	            	title: 'La contraseña debe incluir al menos un número y un caracter.',
	            	buttons: ['OK']
	          	});

	          	loader.dismiss();
	          	alert.present();
         	}else{
	     		this.Serv.getCSRFToken()
			      .subscribe(response=>{
			        this.Serv.login(model, response['_body'])
			        .subscribe(response =>{
			        
			         console.log(response);

			         loader.dismiss();

			         let alert =  this.alertCtrl.create({
			              title: response.message,
			              buttons: ['OK']
			          });

			          alert.present();

			     	  this.app.getRootNav().setRoot(LoginPage);

			       })
			     })
         	}
         }
      }
            
  	}else {

      let alert = this.alertCtrl.create({
        title: 'Por favor complete todos los campos',
        buttons: ['OK']
      });
      alert.present();

    }
  }

}
