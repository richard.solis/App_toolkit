import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SessionService } from '../../services/session.service';
import { Services } from '../../services/services.service';
import * as $ from 'jquery';
import { Content } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ToolPage } from '../tool/tool';
import { DownloadablePage } from '../downloadable/downloadable';
import { ResourcePage } from '../resource/resource';

/**
 * Generated class for the SearchingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searching',
  templateUrl: 'searching.html',
})
export class SearchingPage {

  public results: Array<any> = [];
  public userForm:FormGroup;
  public text_search = {
    text : ''
  };
  public message = '';

  constructor(
     public navCtrl: NavController, 
     public navParams: NavParams,
     public _fb: FormBuilder, 
     public elRef: ElementRef,
     public menu: MenuController,
     public SS: SessionService,
     public loadingCtrl: LoadingController,
     public alertCtrl: AlertController,
     public Serv: Services,
     public app: App,
     private camera: Camera,
   ) {

  }

  ngOnInit() {
      
    this.text_search = this.navParams.get('text');

    this.userForm = this._fb.group({
      text: [this.text_search.text , Validators.required],
      action: 'search'
    });

  }

  ionViewDidLoad() {

    let loader = this.loadingCtrl.create({});

    loader.present(); 

    this.text_search = this.navParams.get('text');
    this.Serv.searchNodes(this.text_search, this.SS.getItem('token'))
      .subscribe(
        response => {
          // console.log(response);

          if(response.message){
             this.message = response.message;
          }else{
            this.message = '';
          }

          loader.dismiss(); 

          this.results = response;

          if(this.results.length > 0){
            this.results.sort((a, b) => {
              if (a.type_content > b.type_content) return -1;
              else if (a.type_content < b.type_content) return 1;
              else return 0;
            });
          }

        }
      )
  }

  goNode(result){

    // console.log(result);

  	if(result.type_content == 'Tool'){
    	// this.navCtrl.setRoot(ToolPage);
      this.navCtrl.push(ToolPage, {
        id_tool: result.nid
      });
  	}else if(result.type_content == 'Descargable'){
 
      this.navCtrl.push(DownloadablePage, {
        id: result.nid
      });

  	}else if(result.type_content == 'Recurso'){
      // this.navCtrl.setRoot(ResourcePage);

      this.navCtrl.push(ResourcePage, {
        id: result.nid
      });
    }

  }

  searching(model,isValid:Boolean){

    let loader = this.loadingCtrl.create({});
    loader.present(); 

    if (isValid) {

      this.Serv.searchNodes(model, this.SS.getItem('token'))
      .subscribe(
        response => {
          // console.log(response);

          if(response.message){
             this.message = response.message;
          }else{
            this.message = '';
          }

          // console.log(this.message);

          this.results = response;

          if(this.results.length > 0){
            this.results.sort((a, b) => {
              if (a.type_content > b.type_content) return -1;
              else if (a.type_content < b.type_content) return 1;
              else return 0;
            });
          }

          loader.dismiss(); 

        }
      )

    }else{
      let alert = this.alertCtrl.create({
        title: 'Por favor complete el campo de busqueda',
        buttons: ['OK']
      });
      alert.present();
      loader.dismiss(); 

    }

  }

}
