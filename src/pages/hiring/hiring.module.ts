import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HiringPage } from './hiring';

@NgModule({
  declarations: [
    HiringPage,
  ],
  imports: [
    IonicPageModule.forChild(HiringPage),
  ],
})
export class ThemesPageModule {}
