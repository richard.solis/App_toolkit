import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Slides, AlertController } from 'ionic-angular';
import { Services } from '../../services/services.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Http } from '@angular/http';
import { TagPage } from '../tag/tag';
import { SessionService } from '../../services/session.service';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ResourcePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resource',
  templateUrl: 'resource.html',
})
export class ResourcePage {

  public resource = {};
  public nid;

  constructor(
     public navCtrl: NavController, 
     public navParams: NavParams, 
     public Services: Services, 
     public _fb: FormBuilder,
     public http: Http,
     public loadingCtrl: LoadingController,
     public alertCtrl: AlertController,
     public SS: SessionService,
     private socialSharing: SocialSharing
) {

  	// this.downloadable = {
  	// 	id: 1,
  	// 	tool: 'Feedback SBI',
  	// 	category: 'Teams',
  	// 	title: 'Forbes: The Best Gift Leaders Can Give: Honest Feedback',
  	// 	url: 'https://www.forbes.com/forbes/welcome/?toURL=https://www.forbes.com/sites/joefolkman/2013/12/19/the-best-gift-leaders-can-give-honest-feedback/&refURL=http://leaderstoolkit.atypax.com/recurso/forbes-best-gift-leaders-can-give-honest-feedback&referrer=http://leaderstoolkit.atypax.com/recurso/forbes-best-gift-leaders-can-give-honest-feedback#1fd9d19c4c2b',
   //      imagen: 'http://leaderstoolkit.atypax.com/sites/default/files/2017-09/Enlace_1.jpg',
  		
  	// }
  }

  ionViewDidLoad() {

    this.nid = this.navParams.get('id');
    
    let loader = this.loadingCtrl.create({});

    // console.log(this.id_tool);
    loader.present();

    this.Services.getDataTool({
      action: 'recurso',
      nodeId: this.nid

    }, this.SS.getItem('token'))
      .subscribe(
        response => {

          loader.dismiss();
          console.log(response);
          this.resource = response;
        }
      )
  }

}
