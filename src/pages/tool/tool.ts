import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Slides, AlertController } from 'ionic-angular';
import { Services } from '../../services/services.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Http } from '@angular/http';
import { TagPage } from '../tag/tag';
import { SessionService } from '../../services/session.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the ToolPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tool',
  templateUrl: 'tool.html',
  providers: [
    Services,
    FormBuilder
  ]
})
export class ToolPage {

  public commentForm:FormGroup;
  public answerForm: FormGroup;

  public name_user = '';
  public tool = {
    comment: [],
    favorite: true
  };
  public loader = this.loadingCtrl.create({});
  public tag;
  public id_tool;
  public user = {
    id: '',
    picture: '',
    uname: '',
    nickname: '',
    nombre_completo: ''
  };
  public show:boolean = false;
  public prevClicked:number = -1;
  public comments = [];
  public errorMessage;
  public image = '';

  public date;
  public day;
  public month;
  public year;
  public date_today;

  @ViewChild(Slides) slides: Slides;

  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams, 
    public Services: Services, 
    public _fb: FormBuilder,
    public http: Http,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public SS: SessionService,
    private socialSharing: SocialSharing,
    private photoViewer: PhotoViewer,
    private iab: InAppBrowser
    ) {
  }

  ngOnInit() {

    this.date =  new Date();

    this.day = this.date.getDate();
    this.month = this.date.getMonth() + 1;
    this.year = this.date.getFullYear();
    this.date_today = this.day + '-' + this.month + '-' + this.year;

    this.user = this.SS.getObject('toolkit.member');

    // this.user.picture = this.SS.getObject('toolkit.member.photo');
    // this.user.uname = this.SS.getObject('toolkit.member.uname');
    this.id_tool = this.navParams.get('id_tool');

    // console.log(this.user.id);

    this.commentForm = this._fb.group({
      comentario: ['', Validators.required],
      uid: this.user.id,
      nodeId: this.id_tool,
      action: 'comment'
    });


    this.answerForm = this._fb.group({
      answer: ['', Validators.required],
    });

    this.Services.login({
        name: this.user.uname,
        pass: atob(this.SS.getItem('token_')),
        action: 'login'
      }, this.SS.getItem('token'))
      .subscribe(response =>{
        // console.log("logueado");
        // loader.dismiss();

        this.SS.setObject('toolkit.member', response.user);
        this.image = response.user.picture;

        // console.log((this.image).indexOf('base64'));

      })

  }

  getComments(){
    // GET COMMENTS OF TOOL

    this.Services.getDataTool({
      action: 'comments',
      nodeId: this.id_tool,
      uid: this.user.id

    }, this.SS.getItem('token'))
      .subscribe(
        response => {

          this.comments = response;
          // this.SS.setObject('toolkit.comments', this.comments);

        },err => {

          this.Services.getDataTool({
            action: 'comments',
            nodeId: this.id_tool,
            uid: this.user.id

          }, this.SS.getItem('token'))
            .subscribe(
              response => {
                
                this.Services.getDataTool({
                  action: 'comments',
                  nodeId: this.id_tool,
                  uid: this.user.id

                }, this.SS.getItem('token'))
                  .subscribe(
                    response => {

                      this.comments = response;

                    }
                  )
              }
            )
        }
      )
  }

  ionViewWillEnter() {

    this.id_tool = this.navParams.get('id_tool');
    
    let loader = this.loadingCtrl.create({});
    let action = 'nodo';

    // console.log(this.id_tool);
    loader.present();

   
    // GET DATA TOOL

    this.Services.getDataTool({
      action: 'nodo',
      nodeId: this.id_tool,
      uid: this.user.id


    }, this.SS.getItem('token'))
      .subscribe(
        response => {
           loader.dismiss();

          // console.log(response);

          this.tool = response;
          this.tag = response.tag;
        }
      )

      this.getComments();

    
  }

  shared(tool){
    // console.log(tool);
    this.socialSharing.share(tool.title, null, null, tool.path);
  }

  goToSlide() {
    this.slides.slideTo(2, 500);
  }


  onSubmit(model:any, isValid:Boolean){
    // console.log(model);

    let loader = this.loadingCtrl.create({});

    // loader.present();

    if (isValid) {
      this.Services.comment(model, this.SS.getItem('token'))
      .subscribe(
        response => {
          
          loader.dismiss();

          this.commentForm = this._fb.group({
            comentario: ['', Validators.required],
            uid: this.user.id,
            nodeId: this.id_tool,
            action: 'comment'
          });

          this.ionViewWillEnter();

        }
      )
    
    }else {
      let alert = this.alertCtrl.create({
        title: 'Por favor complete los campos',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  goTag(tool){

    // console.log(tag);

    this.navCtrl.push(TagPage, {
      tag: tool.tag,
      tag_id: tool.tagId
    });
    
  }

  clicked(index) {// only show clicked img info 
      if(this.comments[this.prevClicked] && this.prevClicked != index) {
        this.comments[this.prevClicked].show = false;
      }
      this.comments[index].show = !this.comments[index].show;
      this.prevClicked = index;
  }

  onSubmitAnswer(model:any, isValid:Boolean, comment){

    // console.log('comment', comment);

    let loader = this.loadingCtrl.create({});

    let params = {
      action: 'answer',
      nodeId: this.id_tool,
      uid: this.user.id,
      comentario: model.answer,
      cid: comment.cidAnswer
    }

    if (isValid) { 
  
      loader.present();

      // console.log(params);
      this.Services.comment(params, this.SS.getItem('token'))
      .subscribe(
        response => {
          
          
          for(var i=0; i<this.comments.length; i++){
            if(comment.cidAnswer == this.comments[i].cidAnswer){
              // console.log(this.comments[i]);

              if(this.user.nombre_completo != ''){
                this.name_user = this.user.nombre_completo;
              }else{

                if(this.user.nickname != ''){
                  this.name_user = this.user.nickname;
                }else{
                  this.name_user = this.user.uname;
                }

              }

              this.comments[i].answer.push({
                body: model.answer,
                userPhoto: this.user.picture,
                userName: this.name_user,
                date: this.date_today
              })

              this.answerForm = this._fb.group({
                answer: ['', Validators.required],
              });
            }
          }

          loader.dismiss();

          // this.getComments();


          // this.ionViewWillEnter();
          console.log(response,"hola")
        }

      )
    
    }else {

      let alert = this.alertCtrl.create({
        title: 'Por favor complete los campos',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  showImage(image){
    this.photoViewer.show(image);
  }

  openUrl(url){

    // console.log(url);

    let target = "_blank";

    this.iab.create(url);
  }

  showFavorite(favorite){

    // console.log(favorite);

    if(favorite){

      this.tool.favorite = false;

      this.Services.assignFavorite({
        action: 'flag',
        nodeId: this.id_tool,
        uid: this.user.id,
        state: 'unflag'
      }, this.SS.getItem('token'))
        .subscribe(
          response => {
            console.log(response,"hola")
            // console.log(response);

          }
        )

    }else{
      this.tool.favorite = true;

      this.Services.assignFavorite({
        action: 'flag',
        nodeId: this.id_tool,
        uid: this.user.id,
        state: 'flag'
      }, this.SS.getItem('token'))
        .subscribe(
          response => {

            // console.log(response);

          }
        )
    }

  }

}
