import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavBarPage } from './nav-bar';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';


@NgModule({
  declarations: [
    NavBarPage,
  ],
  imports: [
    IonicPageModule.forChild(NavBarPage),
  ],
})
export class NavBarPageModule {

	constructor(
		public navCtrl: NavController) {
    }

    goHome(){
  		this.navCtrl.setRoot(HomePage);
    }
}
