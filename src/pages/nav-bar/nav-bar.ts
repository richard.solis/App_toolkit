import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the NavBarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nav-bar',
  templateUrl: 'nav-bar.html',
})
export class NavBarPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NavBarPage');
  }

  goHome(){
  	this.navCtrl.setRoot(HomePage);
  }

  goBack(){
    this.navCtrl.pop(); 
  }

}
