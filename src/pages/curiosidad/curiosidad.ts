import { Component } from '@angular/core';
import { IonicPage, NavController,  NavParams, LoadingController} from 'ionic-angular';
import { Services } from '../../services/services.service';
import { Http } from '@angular/http';
import { SessionService } from '../../services/session.service';
import { ToolPage } from '../tool/tool';
import { AppSettings } from '../../app/app.settings';

/**
 * Generated class for the CuriosidadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-curiosidad',
  templateUrl: 'curiosidad.html',
})
export class CuriosidadPage {

  public curiosidad = [];
  public loader = this.loadingCtrl.create({});
  public image_complete;
  public title;
  public description;

  public serveMessage;

  constructor(public navCtrl: NavController, 
                          public navParams: NavParams,
                          public Serv: Services, 
                          public http: Http,
                          public loadingCtrl: LoadingController,
                          public SS: SessionService,) {
  }

  ionViewDidLoad() {
     let loader = this.loadingCtrl.create({});
    let category = this.navParams.get('category');

    loader.present();
    
     this.Serv.getBlocks({
          action: 'blocks',
          categoryId: AppSettings.ID_CURIOSIDAD
        }, this.SS.getItem('token'))
        .subscribe(response =>{
          this.serveMessage = response.length;
          if(response.length != 0){
             loader.dismiss();
            let block_image = response[0].body;
            this.title = response[1].body;
            this.description = response[2].body;
            let part1 = block_image.split('/sites')[0];
            let path = AppSettings.SERVER;
            let part2 = block_image.split('/sites')[1];
            this.image_complete = part1 + path + 'sites' +  part2;
          }else {
             this.Serv.getBlocks({
              action: 'blocks',
              categoryId: AppSettings.ID_LEADERSHIP
            }, this.SS.getItem('token'))
              .subscribe(response =>{
                 loader.dismiss();
                  let block_image = response[0].body;
                  this.title = response[1].body;
                  this.description = response[2].body;
                  let part1 = block_image.split('/sites')[0];
                  let path = AppSettings.SERVER;
                  let part2 = block_image.split('/sites')[1];
                  this.image_complete = part1 + path + 'sites' +  part2;
              });
            }
          console.log(response,"dasdas");
          


          // Replace path image url

          
          
          

        });

        this.Serv.getMenu({
          action: 'by_categoria',
          categoryId: category.id
          }, this.SS.getItem('token'))
          .subscribe(response =>{
            this.curiosidad = response;
          });

  }

  goTool(id){
    console.log(id);

    this.navCtrl.push(ToolPage, {
      id_tool: id
    });
  }
};
