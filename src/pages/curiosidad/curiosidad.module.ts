import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CuriosidadPage } from './curiosidad';

@NgModule({
  declarations: [
    CuriosidadPage,
  ],
  imports: [
    IonicPageModule.forChild(CuriosidadPage),
  ],
})
export class CuriosidadPageModule {}
