import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Services } from '../../services/services.service';
import { Http } from '@angular/http';
import { SessionService } from '../../services/session.service';
import { ToolPage } from '../tool/tool';
import { AppSettings } from '../../app/app.settings';

/**
 * Generated class for the TeamsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
  providers: [
    Services
  ]
})
export class TeamsPage {

  public teams = [];
  public loader = this.loadingCtrl.create({});
  public image_complete;
  public title;
  public description;
  
  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams, 
    public Serv: Services, 
    public http: Http,
    public loadingCtrl: LoadingController,
    public SS: SessionService) {
  }

  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({});
    let category = this.navParams.get('category');

    loader.present();

    this.Serv.getBlocks({
          action: 'blocks',
          categoryId: AppSettings.ID_TEAMS
        }, this.SS.getItem('token'))
        .subscribe(response =>{
          
          // loader.dismiss();
          let block_image = response[0].body;
          this.title = response[1].body;
          this.description = response[2].body;

          // Replace path image url

          let part1 = block_image.split('/sites')[0];
          let path = AppSettings.SERVER;
          let part2 = block_image.split('/sites')[1];
          
          this.image_complete = part1 + path + 'sites' +  part2;

        })

    this.Serv.getMenu({
          action: 'by_categoria',
          categoryId: category.id
        }, this.SS.getItem('token'))
        .subscribe(response =>{
          loader.dismiss();
          this.teams = response;
          // this.teams.sort((a, b) => {
          //   if (a.title < b.title) return -1;
          //   else if (a.title > b.title) return 1;
          //   else return 0;
          // });
          // console.log(this.leaderships);
        })
  }

  goTool(id){
    console.log(id);

    this.navCtrl.push(ToolPage, {
      id_tool: id
    });
  }

}
