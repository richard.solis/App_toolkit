import { Component, OnInit } from '@angular/core';
import { NavController, MenuController,  LoadingController, AlertController } from 'ionic-angular';
import { HiringPage } from '../hiring/hiring';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { SessionService } from '../../services/session.service';
import { OnboardingPage } from '../onboarding/onboarding';
import { LeadershipPage } from '../leadership/leadership';
import { TeamsPage } from '../teams/teams';
import { AllsPage } from '../alls/alls';
import { FavoritesPage } from '../favorites/favorites';
import { SearchingPage } from '../searching/searching';
import { CuriosidadPage } from '../curiosidad/curiosidad';
import { ResultadosPage } from '../resultados/resultados';
import { Services } from '../../services/services.service';
import { AppSettings } from '../../app/app.settings';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ FormBuilder]
})
export class HomePage{

  public userForm:FormGroup;
  public name_user = '';
  public user = {
    uname: '',
    id: '',
    nickname: ''
  };
  public menu_items= [];
  public block = {};
  public body_complete;

  constructor(
    public navCtrl: NavController, 
    public sessionS: SessionService, 
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public Serv: Services,
    public _fb: FormBuilder,
    public alertCtrl: AlertController,
    ) {

  }

  ngOnInit() {

    this.menu.close(); 
    
    this.userForm = this._fb.group({
      text: ['', Validators.required],
      action: 'search'
    });

  }

  ionViewWillEnter() {

    let loader = this.loadingCtrl.create({});
    this.user = this.sessionS.getObject('toolkit.member');

    // console.log(this.user.uname);

    if(this.user.nickname == ''){
      this.name_user = this.user.uname;
    }else{
      this.name_user = this.user.nickname;
    }

    this.menu.close(); 

    loader.present();

    this.Serv.getBlocks({
          action: 'blocks',
          categoryId: AppSettings.ID_HOME
        }, this.sessionS.getItem('token'))
        .subscribe(response =>{
          
          loader.dismiss();

          let body_block = response[1];
          let part1 = body_block.body.split('/sites')[0];
          let path = AppSettings.SERVER;
          let part2 = body_block.body.split('/sites')[1];
          
          this.body_complete = part1 + path + 'sites' +  part2;

        })

      this.Serv.getMenu({
        action: 'main_menu',
        uid: this.user.id

      }, this.sessionS.getItem('token'))
      .subscribe(response =>{
        // loader.dismiss();
        this.menu_items = response;
        console.log(this.menu_items,"xd")

        if(this.menu_items.length > 0){
          this.menu_items.sort((a, b) => {
            if (a.id < b.id) return -1;
            else if (a.id > b.id) return 1;
            else return 0;
          });
        }

        // console.log(this.menu_items);
      })
  }

  goCategorieDetail(item) {

    console.log(item);

    if(item.id == "1"){
      this.navCtrl.push(HiringPage, {
        category: item
      });
    }else if(item.id == "2"){
      this.navCtrl.push(OnboardingPage, {
        category: item
      });
    }else if(item.id == "3"){
      this.navCtrl.push(LeadershipPage, {
        category: item
      });
    }else if(item.id == "4"){
      this.navCtrl.push(TeamsPage, {
        category: item
      });
    }else if(item.id == "22"){
      this.navCtrl.push(ResultadosPage, {
        category: item
      });
    }else if(item.id == "23"){
      this.navCtrl.push(CuriosidadPage, {
        category: item
      });
    }else if(item.id == "all"){
      this.navCtrl.push(AllsPage, {
        category: item
      });
    }else if(item.id == "favorite"){
      this.navCtrl.push(FavoritesPage, {
        category: item
      });
    }
  }

  searching(model:any, isValid:Boolean){

    if (isValid) {
      this.navCtrl.push(SearchingPage, {
        text: model
      }) 
    }else{
      let alert = this.alertCtrl.create({
        title: 'Por favor complete el campo de busqueda',
        buttons: ['OK']
      });
      alert.present();
    }
    
  }
}
