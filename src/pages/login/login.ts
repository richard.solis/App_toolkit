import { Component, OnInit, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HomePage} from "../home/home";
import { RegisterPage} from "../register/register";

import { SessionService } from '../../services/session.service';
import { Services } from '../../services/services.service';
import * as $ from 'jquery';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [ FormBuilder]
})
export class LoginPage implements OnInit{

	public userForm:FormGroup;
  public contextInputPass; 
  public valueAttr = 'password';
  public iconShowPass = true;
  public iconHiddenPass = false;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public _fb: FormBuilder, 
    public elRef: ElementRef,
    public menu: MenuController,
    public session: SessionService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public Serv: Services,
    public app: App
    )
     {
  }

  ngAfterViewInit() {
    this.contextInputPass = this.elRef.nativeElement.querySelector('#pass')
    console.log(this.contextInputPass);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.menu.swipeEnable(false);
  }

  ngOnInit() {

    this.menu.close(); 
    
  	this.userForm = this._fb.group({
  		dni: ['', Validators.required],
  		pass: ['', Validators.required],
  		remember: [false],
      action: 'login'
  	});

    if (this.session.getItem('token')) {
      this.app.getRootNav().setRoot(HomePage);   
    }

  }

  onSubmit(model:any, isValid:Boolean){
  	// console.log(model);
  	if (isValid) {

      // this.app.getRootNav().setRoot(HomePage);

  		// console.log(model);
      let loader = this.loadingCtrl.create({});
      loader.present();
      this.Serv.getCSRFToken()
      .subscribe(response=>{
        this.Serv.login({
          name: model.dni,
          pass: model.pass,
          action: 'login'
        }, response['_body'])
        .subscribe(response =>{
          if(response.message){
            loader.dismiss();
            let alert =  this.alertCtrl.create({
              title: 'Datos incorrectos',
              buttons: ['OK']
            });
            alert.present();
            return;
          }

          loader.dismiss();

          if(response.user){
            this.session.setObject('toolkit.member', response.user);
            this.session.setItem('token', response.user.token);
            this.session.setItem('token_', btoa(model.pass));
            this.app.getRootNav().setRoot(HomePage);
          }

        })
      })
  		// this.navCtrl.setRoot(HomePage);
  	}else {
      let alert = this.alertCtrl.create({
        title: 'Por favor complete todos los campos',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  showPass(){

    // console.log($('.input.password input').attr('type'));

    let type = $('.input.password input').attr('type');

      if (type == "password") {
        $('.input.password input').attr('type', 'text');
          this.iconShowPass = false;
          this.iconHiddenPass = true;

      } else {
        $('.input.password input').attr('type', 'password');
          this.iconShowPass = true;
          this.iconHiddenPass = false;
      }

  }

  goRegister(){
     this.app.getRootNav().setRoot(RegisterPage);

  }

}
