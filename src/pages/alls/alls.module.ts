import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllsPage } from './alls';

@NgModule({
  declarations: [
    AllsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllsPage),
  ],
})
export class AllsPageModule {}
