import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions, Http } from '@angular/http';
import { AppSettings } from '../app/app.settings';
import { Observable } from "rxjs/Rx";

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class Services {

    constructor(
        private http: Http) { }

    public getCSRFToken() {
        return this.http.get(AppSettings.SERVER + AppSettings.SERVER_TOKEN)
        .map((res: Response) => {
            let response = res;
            return response;
        });
    }

    public login(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.LOGIN, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public getMenu(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public getDetailCategory(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public getBlocks(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public getDataTool(params:Object, CSRFToken:String) {

        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public comment(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
           
            const response = res.json();
            return response;
        })
        .catch((e) => {
            return Observable.throw(
              new Error(`${ e.status } ${ e.statusText }`)
            );
        });
    }

     public assignFavorite(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public filterTag(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public getDataUser(CSRFToken:String){
        // const body = JSON.stringify(params);
        const headers = new Headers({
            'Authorization': 'Bearer ' + CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.get(AppSettings.SERVER + AppSettings.LOGIN, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

    public updateUser(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.LOGIN, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        })
        .catch((e) => {
            return Observable.throw(
              new Error(`${ e.status } ${ e.statusText }`)
            );
        });
    }

    public searchNodes(params:Object, CSRFToken:String){
        const body = JSON.stringify(params);
        const headers = new Headers({
            'X-CSRF-Token': CSRFToken,
            'Content-Type': 'application/json',
        });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(AppSettings.SERVER + AppSettings.CATEGORY, body, options)
        .map((res:Response) => {
            const response = res.json();
            return response;
        });
    }

}
